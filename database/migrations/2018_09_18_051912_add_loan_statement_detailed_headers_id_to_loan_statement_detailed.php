<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLoanStatementDetailedHeadersIdToLoanStatementDetailed extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loan_statement_detaileds', function (Blueprint $table) {
            //
            $table->integer('loan_statement_detailed_headers_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loan_statement_detaileds', function (Blueprint $table) {
            //
        });
    }
}
