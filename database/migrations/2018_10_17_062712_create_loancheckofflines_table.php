<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoancheckofflinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      
        Schema::create('loancheckofflines', function (Blueprint $table) {
                 $table->increments('id');
                 $table->bigInteger('recid');
                 $table->integer('loancheckoffheaders_id');
                 $table->string('accountnum');            
                 $table->string('nationalidno');
                 $table->float('amount');            
                 $table->float('amountexpected');
                 $table->string('checkoffbatchno');            
                 $table->string('comments');
                 $table->date('documentdate');            
                 $table->string('documentno');
                 $table->tinyInteger('found');            
                 $table->string('guarantor');
                 $table->date('lastpaymentdate');            
                 $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loancheckofflines');
    }
}
