<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLminstitutionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lminstitution', function (Blueprint $table) {
            $table->increments('id');
            $table->string('country');
            $table->string('institutionCode');
            $table->string('institutionName');
            $table->string('password');
            $table->string('vendaccount');
            $table->integer('institutionStaus');
            $table->string('mobileno');
            $table->integer('openday');
            $table->integer('openmonth');
            $table->integer('openyear');
            $table->string('institutionshortcode');
            $table->integer('institution_type');
            $table->integer('schoolType');
            $table->string('modifiedDateTime');
            $table->string('modifiedBy');
            $table->string('createdDateTime');
            $table->string('createdBy');
            $table->string('dataAreAid');
            $table->integer('recVersion');
            $table->bigInteger('partition');
            $table->bigInteger('recId');
            $table->string('email');
            $table->integer('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lminstitution');
    }
}
