<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanbillingheadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loanbillingheaders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('batchno');
            $table->string('officer');
            $table->string('empcode');
            $table->date('datecreated');
            $table->date('datedue');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loanbillingheaders');
    }
}
