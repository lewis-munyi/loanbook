<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCalculatedColumnsToLoans extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loans', function($table) {
            $table->float('principal_repaid')->default(0);
            $table->float('interest_charged')->default(0);
            $table->float('interest_repaid')->default(0);
            $table->float('ledger_charged')->default(0);
            $table->float('ledger_repaid')->default(0);
            $table->float('insurance_charged')->default(0);
            $table->float('insurance_repaid')->default(0);
            $table->float('penalty_charged')->default(0);
            $table->float('penalty_repaid')->default(0);
            $table->float('waivers_amount')->default(0);
            $table->float('running_balance')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
