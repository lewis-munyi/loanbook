@extends('layouts.app')
@section('nav-left')
	<a class="navbar-brand" href="#"><h3>Legacy Bills</h3></a>
@endsection
@section('nav-search')
	<form class="legacy-search form-inline my-2 my-lg-0" id="searchForm" method="POST" id="frmlegacy" action="{{route('createlegacylines')}}" enctype="multipart/form-data">
		{{ csrf_field() }}
		<div class="row">
			<div class="col-sm-6">
				<input class="form-control mr-sm-2" type="search" placeholder="Enter ID number" aria-label="Search" v-model="searchTerm" name="idno"  required="required" value="{{$idno}}">
			</div>
			
		</div>
		<button class="btn btn-outline-light my-2 mr-5" id="btnValidate" type="submit">Submit</button>
	</form>
@endsection

@section('title', 'Legacy Data')
@section('content')

<div class="page-content">

<div class="row">
	<div></div>
	<div class="col-sm-12">
		@if($status=='notfound')
			  <div class="alert alert-danger alert-dismissible fade show">
			    <button type="button" class="close" data-dismiss="alert">&times;</button>
			    <strong>Record not Found!</strong>  ID Number {{$idno}} Has not benefited from any HELB loan product!! 
			  </div>
		@elseif($status=='')
			  <div class="alert alert-info alert-dismissible fade show">
			    <button type="button" class="close" data-dismiss="alert">&times;</button>
			    <strong>Enter ID Number to search HELB loan product!! </strong>			    
			  </div>		  
		@else
			  <div class="alert alert-success alert-dismissible fade show">
			    <button type="button" class="close" data-dismiss="alert">&times;</button>
			    <strong>Record Found!</strong>  ID Number {{$idno}} Has benefited from HELB loan product!! 
			  </div>
	    @endif
	</div>


<div class="col-sm-12">
	@include('loanstatement.legacystatement')
</div>
	
	
</div>
</div>

@endsection

@section('page-scripts')

<script>
    $(document).ready(function() {
        $('#tblStatement').DataTable();
    });



$(document).ready(function () {

    $("#myForm").validate({ // initialize the plugin
        // any other options,
        onkeyup: false,
        rules: {
            //...
        },
        messages: {
            //...
        }
    });

    $("#frmRemittance").ajaxForm({ // initialize the plugin
        // any other options,
        beforeSubmit: function () {
        	$('#remitOverlay').show();
            
        },
        success: function (response) {
        	//$('#remitOverlay').fadeOut();
        	if(response.status === false){
        		$('#remitOverlay').html('<h4>'+response.message+'</h4>')
        		var errs = response.errors;
        		console.log(errs);
        		for(i=0;i<errs.length;i++){
        			$('#remitOverlay').append('<p class="text-danger">'+errs[i]+'</p>');
        			$('#remitOverlay').append('<button class="btn btn-primary" onclick="hideOverlay()">OK</button>');
        		}
        	}else{
        		//$('#overlayContent').html(response.message);
        		window.location.href = response.url;
        	}
            console.log(response);
        }
    });

});

function hideOverlay(){
	$('#remitOverlay').html('<h3 class="loading">Working...</h3>').hide();
}

</script>

@endsection
