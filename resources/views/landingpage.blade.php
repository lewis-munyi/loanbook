@extends('layouts.app')
@section('nav-left')
  <a class="navbar-brand" href="#"><h3>Welcome.</h3></a>
@endsection
@section('title', 'Karibu')
@section('content')
  <style>
    .centered {
      position: fixed;
      top: 50%;
      left: 50%;
      /* bring your own prefixes */
      transform: translate(-30%, -50%);
    }
    .landing-title-centered {
      position: fixed;
      top: 20%;
      left: 50%;
      /* bring your own prefixes */
      transform: translate(-35%, -50%);
    }
  </style>
<div class="container-fluid text-center">
  <div class="landing-title-centered">
    <h2 class="text-uppercase">Reports Portal</h2><br><br>
   <!--  <a class="btn btn-success btn-outline-dark" href="/home">Proceed to dashboard</a> -->
  </div>
  
  <img src="{{asset('img/logo.png')}}" class="centered" alt="">
</div>
@endsection
