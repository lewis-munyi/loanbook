@extends('layouts.app')
@section('nav-left')
    <a class="navbar-brand" href="#"><h3>Collections</h3></a>
@endsection
@section('nav-search')
    <form method="POST"  action="{{route('collections-report')}}" enctype="multipart/form-data" class="statements-search form-inline my-2 my-lg-0" id="searchCollectionReport" autocomplete="off">
        {{ csrf_field() }}
        <div class="row">
        <div class="col-sm-9">
                <!-- <input type="text" class="form-control mr-3" name="datefilter" value="" placeholder="Select date range" id="daterange" required/> -->

        <div class="input-group input-daterange ">         
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
                <input id="startDate1" placeholder="From Date" name="startDate" type="text" class="form-control" readonly="readonly"  value="{{ old('startDate') }}" required>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
                </span>
                <div class="text-white mt-auto"><span>&nbsp; to &nbsp;</span></div>
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
                <input id="endDate1" placeholder="To Date" name="endDate" type="text" class="form-control" readonly="readonly"  value="{{ old('endDate') }}" required>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span></span>
                </span>
        </div>



            &nbsp;&nbsp;
          @if ($errors->has('startDate'))
                <span class="has-error help-block badge-danger">{{ $errors->first('startDate') }}</span>
            @endif
        @if ($errors->has('endDate'))
                <span class="has-error help-block badge-danger">{{ $errors->first('endDate') }}</span>
            @endif
       
            </div>
           <!--  <div class="col-sm-3">
                <select class="custom-select ml-3" id="sector" name="sector">
                    <option value="">Sector</option>
                    <option value="Agr Organs" >Agricultural Organization</option>
                    <option value="Diplo Miss">Diplomatic Missions</option>
                    <option value="Edu Instit" >Educational Institutions and schools</option>
                    <option value="Fin Insti"> Financial Institutions</option>
                    <option value="Insu Comp">Insurance Companies</option>
                    <option value="Judiciary" >Judiciary</option>
                    <option value="Man Sector">Manufacturing Sector</option>
                    <option value="Ministries" >Ministries ,depts Gok</option>
                    <option value="NGOs"> NGOs</option>
                    <option value="Others">Others</option>
                    <option value="Parastatal" >Parastatals</option>
                    <option value="PSC">Parliamentary Service Commission</option>
                    <option value="Profession" >Professionals</option>
                    <option value="Ser Indust"> Service Industries</option>
                    <option value="TSC">Teachers Service Commission</option>
                </select>
            </div> -->
            <div class="col-sm-3">
              <!--   <select class="custom-select" id="loanproduct" name="loanproduct">
                    <option value="">Loan Product</option>
                   

</option value="FD">  Afya Elimu</option>
<option value="AICKJ">   AIC KIJABE REVOLVING FUND</option>
<option value="AC">   Awendo Education Fund-certificate</option>
<option value="AU">   Awendo Education Fund-Undergraduate Studies - Degree </option>
<option value="SU">   Busia Count Revolving Fund</option>
<option value="CD">   Community  Health Promotion Fund</option>
<option value="CE">   Continuing Education Salaried-Undergraduate</option>
<option value="DU">   Digital Data Divide Undergraduate</option>
<option value="EASTLANDS">    EAST LANDS</option>
<option value="FU">   Ford foundation fund</option>
<option value="HELF">     HELF</option>
<option value="IU">   Igembe North- Degree</option>
<option value="KCAEF">    KAKAMEGA COUNTY AFYA ELIMU REVOLVING FUND</option>
<option value="GU">   Kakamega county fund</option>
<option value="KC">   Karachuonyo Education Fund  -certificate</option>
<option value="KD">   Karachuonyo Education Fund  -Diploma</option>
<option value="KU">   Karachuonyo Education Fund - Degree</option>
<option value="RM">   KRA STAFF LOAN</option>
<option value="PG">   Post Graduate Studies</option>
<option value="SM">   Postgraduate Studies - Masters</option>
<option value="TM">   Postgraduate Studies - Masters & PHD (TRF)</option>
<option value="NM">   Postgraduate Studies - Masters Degree</option>
<option value="RP">   Postgraduate Studies - Phd Degree</option>
<option value="PI">   Postgraduate Studies- Masters/Phd</option>
<option value="SB">   Simba Foundation Revolving Fund</option>
<option value="STKVTF">   St Kizito Vocational Training Fund</option>
<option value="TV">   TAITA TAVETA COUNTY REVOLVING FUND</option>
<option value="DP">   Tertiary Studies  -diploma</option>
<option value="IA">   Tertiary Studies -artisan</option>
<option value="GC">   Tertiary Studies -certificate</option>
<option value="IC">   Tertiary Studies -certificate</option>
<option value="SC">   Tertiary Studies -certificate</option>
<option value="AD">   Tertiary Studies -diploma</option>
<option value="DD">   Tertiary Studies -diploma</option>
<option value="GD">   Tertiary Studies -diploma</option>
<option value="ID">   Tertiary Studies -diploma</option>
<option value="SD">   Tertiary Studies -diploma</option>
<option value="WD">   Tertiary Studies -diploma</option>
<option value="VD">   Tertiary studies for Tvets</option>
<option value="THR">  THARAKA CONSTITUENCY REVOLVING FUND</option>
<option value="DT">   Tigania West CDF -diploma</option>
<option value="UT">   Tigania West Undergraduate</option>
<option value="TP">   Training Revolving Fund - Phd Degree(TRF)</option>
<option value="TU">   Training Revolving Fund (TRF)</option>
<option value="UG">   Undergraduate Studies</option>
<option value="NU">   Undergraduate Studies - Degree</option>
<option value="RU">   Undergraduate Studies - Degree</option>
<option value="WU">   Undergraduate Studies - Degree</option>
                </select> -->
                 <button class="btn btn-outline-light my-2 mr-5" type="submit">Load Report</button>
            </div>
        </div>
       


    </form>
@endsection

@section('content')
    <div>
        @if(count($persectors))
       <div class="container-fluid">
    
   
    </div>
    <br>
    {{--Summaries--}}
    <section class="section">
        
        <div class="row">
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body widget">
                        <span class="widget-label">All Collections</span>
                        <h3>{{number_format(($summary->checkoff->amount + $summary->mpesa->amount),2)}}</h3>
                        <h4></h4>

                        <div>
                           
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body widget">
                        <span class="widget-label">From Employers</span>
                        <h3>{{number_format($summary->checkoff->amount,2)}}</h3>
                        <h4>{{number_format($summary->checkoff->amount*100/($summary->checkoff->amount + $summary->mpesa->amount),2)}}%</h4>

                        <div>
                            <a href="{{route('loans')}}" class="badge badge-success">View List</a>
                            <a href="" class="badge badge-danger"></a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body widget">
                        <span class="widget-label">From MPESA</span>
                        <h3>{{number_format($summary->mpesa->amount,2)}}</h3>
                        <h4>{{number_format($summary->mpesa->amount*100/($summary->checkoff->amount + $summary->mpesa->amount),2)}}%</h4>
                         <a href="{{route('cleared-loans')}}" class="badge badge-secondary"> View List </a>
                    </div>
                </div>
            </div>

           
        </div>
    </section>

            <br>
    <section class="section">
        <div class="row">
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Collections per sector</h4>
                    </div>
                    <div class="card-body">
                         <table class="table">
                        <thead>
                            <th>Sector</th>
                            <th>Amount</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Self</td>
                                <td>{{number_format($summary->mpesa->amount,2)}}</td>
                            </tr>
                             @if(count($persectors))
                           @foreach($persectors as $key => $p)
                            <tr>
                                <td>{{$p->CUSTCLASSIFICATIONID}}</td>
                                <td>{{number_format($p->amount,2)}}</td>
                            </tr>
                            @endforeach
                       @endif
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>


            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Collections per product</h4>
                    </div>
                    <div class="card-body">
                        <table class="table">
                        <thead>
                            <th>Product </th>
                            <th>Amount</th>
                        </thead>
                        <tbody>
                           
                             @if(count($prods))
                           @foreach($prods as $key => $prod)
                            <tr>
                                <td>{{$prod->loanproductcode}}</td>
                                <td>{{number_format($prod->amount,2)}}</td>
                            </tr>
                            @endforeach
                       @endif
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>

        </div>
    </section>
    @endif
</div>


    </div>
@endsection
@section('page-scripts')
    <script type="text/javascript">
        //Datepicker
        $(document).ready(function() {
            $('.input-daterange').datepicker({});
        });
    </script>
@endsection