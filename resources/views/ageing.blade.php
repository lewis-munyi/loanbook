@extends('layouts.app')
@section('nav-left')
    <a class="navbar-brand" href="#"><h3>Loan Ageing</h3></a>
@endsection
@section('nav-search')
    <form class="statements-search form-inline my-2 my-lg-0" id="searchForm" method="POST" id="frmclearanceprojection" action="{{route('ageingresults')}}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-sm-5">
                <select class="custom-select  " id="loanstatus" name="loanstatus" required>
                    <option value="">Select loan status</option>
                    <option >All Loans</option>
                    <option >Cleared</option>
                    <option >Dormant</option>
                    <option >Default</option>
                    <option >Performing</option>
                </select>
            </div>
            <div class="col-sm-5">
                <select class="custom-select  mr-3" id="ageselect" name="ageing" required>
                    <option value="" selected>Choose age value</option>
                </select>
            </div>
        </div>
         <button class="btn btn-outline-light my-2 mr-5" id="btnValidate" type="submit">Get Loans</button>
    </form>
@endsection

@section('title', 'Loan Ageing')
@section('content')
    <div class="row mt-3 mb-3">
        <div class="col col-sm-12 col-lg-2 offset-5 pull-right">
            <!-- Example split danger button -->
            @if(count($results))
            <div class="btn-group">
                <button type="button" class="btn btn-success btn-lg">Download </button>
                <button type="button" class="btn btn-success dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <div class="dropdown-menu">
                    
                    <a href="{{route('ageingcsv', ['ageing' => $params->ageing, 'status' => $params->status])}}" class="btn btn-success btn-block btn-export dropdown-item">Export CSV</a>
                    <div class="dropdown-divider"></div>
                    <a href="{{route('ageingpdf')}}" target="_blank" class="btn btn-success btn-block btn-export dropdown-item">Export PDF</a>
                </div>
            </div>
            @endif
        </div>
    </div>


    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <table class="table">
                        <thead>
                        <th>SNo.</th>
                        <th>Name</th>
                        <th>ID No.</th>
                        <th>Admission No.</th>
                        <th>University</th>
                        <th>Product</th>
                        <th>Academic Yr.</th>
                        <th>Principal</th>
                        <th>Amount Paid</th>
                        <th>Running Balance</th>
                        </thead>
                        <tbody>

                        @if(count($results))
                            @foreach($results as $key => $loan)
                                <tr>
                                    <td>{{$loan->loan_serial_num}}</td>
                                    <td>{{$loan->username}}</td>
                                    <td>{{$loan->id_no}}</td>
                                    <td>{{$loan->student_reg_num}}</td>
                                    <td>{{$loan->university}}</td>
                                    <td>{{$loan->loan_category_code}}</td>
                                    <td>{{$loan->academic_year}}</td>
                                    <td>{{number_format($loan->principal_disbursed)}}</td>
                                    <td>{{number_format($loan->principal_repaid)}}</td>
                                    <td>{{number_format($loan->outstanding_balance)}}</td>
                                </tr>
                            @endforeach
                        @endif

                        </tbody>
                    </table>

                    @if(count($results))
                    <nav>
                        <ul class="pagination justify-content-center">

                            {{$results->links('vendor.pagination.bootstrap-4')}}

                        </ul>
                    </nav>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->

@endsection

@section('page-scripts')
    <script type="text/javascript">
        $(function(){
            $('ul.pagination li a').click(function(e){
                //e.preventDefault();
            });
        });
        var i;
        for (i = 1; i <= 20; i++){
           var option = document.createElement("option");
            option.value = i;
            option.text = i;
            document.getElementById("ageselect").appendChild(option);}
    </script>
@endsection

