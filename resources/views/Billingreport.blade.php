@extends('layouts.app')
@section('nav-left')
    <a class="navbar-brand" href="#"><h3>Bill performance</h3></a>
@endsection
@section('nav-search')
    <form method="POST"  action="{{route('billing-report')}}" enctype="multipart/form-data" class="statements-search form-inline my-2 my-lg-0" id="searchCollectionReport" autocomplete="off">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-sm-4">
                <input type="text" class="form-control" name="datefilter" value="{{$tarehe}}" placeholder="Select date range" id="daterange" required/>
            </div>
            <div class="col-sm-4">
                <input type="text" class="form-control" id="empcode" name="empcode" value="{{$empcode}}" placeholder="Employer Code" >
            </div>
            <div class="col-sm-4">
                <select class="custom-select mr-3" id="officer" name="officer">
                    <option value="">Officer</option>
                    <option value="ktubei">K. Tubei</option>
                    <option value="jsikolia">J Sikolia</option>
                    <option value="jkiptoon">K. Kiptoon</option>
                    <option value="jatieno"> J. atieno</option>
                    <option value="ngakii">N. Gakii</option>
                    <option value="rngali">R. Ngali</option>
                </select>
            </div>
        </div>
        <button class="btn btn-outline-light my-2 ml-2 mr-5" type="submit">Load Report</button>
    </form>
@endsection

@section('content')
    <div>
        @if(count($billingheaders))
       <div class="container-fluid">
    
   
    </div>
    <br>
    {{--Summaries--}}
    <section class="section">
        
        <div class="row">
          

            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body widget">
                        <span class="widget-label">Total Bills</span>
                        <h4>Employers Billed:{{count($billingheaders)}}</h4>
                        <h3>No. of Bills:{{$h+$noth}}</h3>

                        <div>
                    @if($h+$noth>0)
                            <a href="{{route('loans')}}" class="badge badge-success">View List</a>
                            <a href="" class="badge badge-danger"></a>
                    @else
                        <span class="widget-label">List could not be loaded</span>
                    @endif
                        </div>
                    </div>
                </div>
            </div>

<div class="col-sm-4">
                <div class="card">
                    <div class="card-body widget">
                        <span class="widget-label">Bills Honored</span>
                        <h3>No. {{$h}}</h3>
                        <h4>{{number_format($h*100/($h+$noth),2)}}%</h4>
                        <div>
                    @if($h>0)
                          <button  type="button"   class="btn btn-success" data-toggle="collapse" data-target="#honored">View list of bills not honored</button>
                    @else
                        <span class="widget-label">List could not be loaded</span>
                    @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body widget">
                        <span class="widget-label">Bills Not honored</span>
                        <h3>No. {{$noth}}</h3>
                        <h4>{{number_format($noth*100/($h+$noth),2)}}%</h4>
                        @if($noth>0)
                          <button  type="button"   class="btn btn-danger" data-toggle="collapse" data-target="#nothcountonored">View list of honored bills</button>
                        @else
                          <span class="widget-label">List could not be loaded</span>
                        @endif
                    </div>
                </div>
            </div>

           
        </div>

<div class="card">
    
   <div class="card-body">           
            <div id="nothcountonored" class="collapse">
                @include('billsnothonored') 
            </div> 
          <div id="honored" class="collapse">
                @include('billshonored') 
            </div> 

        </div>
</div>
</section>

       @else
<div class="warning">No data found for the specified filters</div>
    
    
    @endif
</div>


    </div>
@endsection
@section('page-scripts')
    <script type="text/javascript">
        //Datepicker
        $(document).ready(function() {
            $('.input-daterange').datepicker({});
        });
    </script>
@endsection