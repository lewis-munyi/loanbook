
@if($status=='found')
<div class="col-sm-8">

		<div class="card">
			<div class="card-header">
				<h4>Below are the details you wish to view</h4>
			</div>
			<div class="card-body">

			<div id="legacyResults">
				
						<table class="table" colspan="8">
						<thead>
							<h1>Historical Bills</h1>							
						</thead>
						
						@foreach ($bills as $rec)
						
						<tbody class="container-fluid">
							
							<td >								
								<tr><td>NATIONALIDNO</td><td>{{ $rec->NATIONALIDNO }}</td></tr>
								<tr><td>ACCOUNTNUM</td><td>{{ $rec->ACCOUNTNUM }}</td></tr>
								<tr><td>LOANSERIALNO.</td><td>{{ $rec->LOANSERIALNO }}</td></tr>
								<tr><td>LOAN PRODUCTCODE</td><td>{{ $rec->LOANPRODUCTCODE }}</td></tr>
								<tr><td>PRINCIPALBALANCE</td><td>{{ $rec->PRINCIPALBALANCE }}</td></tr>
								<tr><td>INTERESTBALANCE</td><td>{{ $rec->INTERESTBALANCE }}</td></tr>
								<tr><td>PENALTYBALANCE</td><td>{{ $rec->PENALTYBALANCE }}</td></tr>
								<tr><td>LEDGERFEEBALANCE</td><td>{{ $rec->LEDGERFEEBALANCE }}</td></tr>
								<tr><td>INSURANCEBALANCE</td><td>{{ $rec->INSURANCEBALANCE }}</td></tr>
								<tr><td>OLDIDVALUE</td><td>{{ $rec->OLDIDVALUE }}</td></tr>
								<tr><td>FLAG</td><td>{{ $rec->IDFLAG }}</td></tr>								
							</td>
							
						</tbody>
						@endforeach
								</table>
								
			</div>
			
			

			
	
		
			
		</div>
		</div>
	</div>
	@else
	@endif