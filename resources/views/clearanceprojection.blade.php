@extends('layouts.app')
@section('nav-left')
    <a class="navbar-brand" href="#"><h3>Loan Clearance Projection</h3></a>
@endsection
@section('nav-search')
    <form class="statements-search form-inline my-2 my-lg-0" id="searchForm" method="POST" id="frmclearanceprojection" action="{{route('clearanceprojectionresults')}}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-sm-6">
                <input class="form-control mr-sm-2" type="number" placeholder="Enter number of months" aria-label="Search" v-model="searchTerm" name="noofmonths">
            </div>
        </div>
         <button class="btn btn-outline-light my-2 mr-5" id="btnValidate" type="submit">Get Loans</button>
    </form>
@endsection

@section('title', 'Loan clearance Projection')
@section('content')

<div class="page-content">

<div class="row">
@if(count($results))

<div class="col-sm-12">

        <div class="card">
            <div class="card-header">
                <h4>Loans Clearing in {{$noofmonths}} {{$month}}</h4><hr />
                <h4> Total No.{{count($results)}} :: Expected Amount:{{number_format($results->sum('outstanding_balance'),2)}}</h4>
            </div>
            <div class="card-body">
                <p> <a href="{{route('clearanceprojection-csv')}}" target="_blank" class="btn btn-primary"><i class="icon dripicons-download"></i> Get file in CSV</a></p>
                       
            <table class="table" id="tblStatement">
                <thead>
                    <th>Name</th>
                    <th>ID Number</th>
                    <th>Phone No.</th>
                    <th>Email</th>
                    <th>Serial No.</th>
                    <th>Product</th>                    
                    <th>Principal Loan</th>
                    <th>Outstanding Balance</th>
                </thead>
                <tbody>
            @foreach($results as $result)
                <tr>
                    <td>{{$result->username}}</td>
                    <td>{{$result->id_no}}</td>
                    <td>{{$result->phone_num}}</td>
                    <td>{{$result->email}}</td>
                    <td>{{$result->loan_serial_num}}</td>
                    <td>{{$result->loan_category_code}}</td>
                    <td>{{number_format($result->principal_disbursed,2)}}</td>
                    <td>{{number_format($result->outstanding_balance,2)}}</td>
                </tr>
            @endforeach
                </tbody>
            </table>
            </div>
        </div>
    </div>
    @endif
</div>
</div>

@endsection

@section('page-scripts')

<script>
    $(document).ready(function() {
        $('#tblStatement').DataTable();
    });
$(document).ready(function () {
    $("#myForm").validate({ // initialize the plugin
        // any other options,
        onkeyup: false,
        rules: {
            //...
        },
        messages: {
            //...
        }
    });
    $("#frmRemittance").ajaxForm({ // initialize the plugin
        // any other options,
        beforeSubmit: function () {
            $('#remitOverlay').show();
        },
        success: function (response) {
            //$('#remitOverlay').fadeOut();
            if(response.status === false){
                $('#remitOverlay').html('<h4>'+response.message+'</h4>')
                var errs = response.errors;
                console.log(errs);
                for(i=0;i<errs.length;i++){
                    $('#remitOverlay').append('<p class="text-danger">'+errs[i]+'</p>');
                    $('#remitOverlay').append('<button class="btn btn-primary" onclick="hideOverlay()">OK</button>');
                }
            }else{
                //$('#overlayContent').html(response.message);
                window.location.href = response.url;
            }
            console.log(response);
        }
    });

});
function hideOverlay(){
    $('#remitOverlay').html('<h3 class="loading">Working...</h3>').hide();
}
</script>

@endsection
