@extends('layouts.app')
@section('nav-left')	
    <a class="navbar-brand" href="#"><h3>Collections</h3></a>
@endsection
@section('nav-search')
@endsection
@section('content')
<div class="row">
	<div class="col-sm-7">
		<div class="card">
		<div class="card-body" id="reportsApp">
			<div v-if="loading" class="preloader"></div>
			<div class="filter">
				<form onsubmit="return false;" v-on:submit="loadDataFromAPI">
					<input type="text" v-model="ageing">
					<input type="text" v-model="loan_status">
					<button class="btn btn-default btn-sm">Search</button>
				</form>
				<a href="javascript:void(0)" v-on:click="helloWorld('Hello there')">Please click me</a>
			</div>
		<table class="table">
		<thead>
			<th>#</th>
			<th>Name</th>
			<th>Acc</th>
			<th>ID No.</th>
		</thead>
		<tbody>
			<tr v-for="user in users">
				<td>@{{user.rec_id}}</td>
				<td>@{{user.username}}</td>
				<td>@{{user.account_num}}</td>
				<td>@{{user.id_no}}</td>
			</tr>
		</tbody>
	</table>
			</div>
		</div>
	</div>
</div>
@endsection

@section('page-scripts')
<script type="text/javascript">
	var reportList = new Vue({
		el: '#reportsApp',
		data: {
			loading: false,
			ageing: null,
			loan_status: null,
			users: []
		},
		mounted(){
			// on load
			this.loadDataFromAPI();
		},
		methods: {
			loadDataFromAPI: function(){
				var self = this;
				///get the data from the server/api
				//show preloader
				self.loading = true;
				axios.post("{{route('report')}}", {
						'ageing' : 10,
						'limit' : self.ageing,
						'status' : 'Defaulted'
				}).then(function (response) {
				    // handle success
				    //console.log(response.data);
				    self.users = response.data;
				    //hide the preloader
				    self.loading = false;
				  })
				  .catch(function (error) {
				    // handle error
				    console.log(error);
				    //hide the preloader
				  })
				  .then(function () {
				    // always executed
				  });
			},
			helloWorld: function(par){
				console.log(par);
			}
		}
	});
</script>
@endsection