
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('moment');
require('./daterangepicker');
window.Vue = require('vue');
require('./bootstrap-datepicker.min.js');
require('./jquery.validate.min');
// require('./d3.min');
// require ('d3');
// var c3 = require('./c3.min');
require('./main');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.component('example-component', require('./components/ExampleComponent.vue'));
// const app = new Vue({
//     el: '#app'
// });

// Service worker config is found in webpack.mix.js
if ('serviceWorker' in navigator ) {
    window.addEventListener('load', function() {
        navigator.serviceWorker.register('/service-worker.js').then(function(registration) {
            // Registration was successful
            // console.log('ServiceWorker registration successful with scope: ', registration.scope);
        }, function(err) {
            // registration failed :(
            console.error('ServiceWorker registration failed: ', err);
        });
    });
}
// Preloader
$(document).ready(()=>{
    document.getElementById('preloader').style.display = 'none';
    document.getElementById('body').style.display = 'block';
});

//Enable Pagination
$(function(){
    $('ul.pagination li a').click(function(e){
        //e.preventDefault();
    });
});

// Old Datepicker
$(document).ready(function() {
    $('.input-daterange').datepicker({});
});

$(function() {
    $('input[name="datefilter"]').daterangepicker({
        opens: 'left',
        linkedCalendars: false,
        "locale": {
            format: "DD/MM/YYYY",
            separator: ".",
            cancelLabel: 'Clear',
            fromLabel: "From",
            toLabel: "To",
        },
        showDropdowns: true,
        autoUpdateInput: false
    }, function(start, end) {
        console.log("A new date selection was made: " + start.format('DD-MM-YYYY') + ' to ' + end.format('DD-MM-YYYY'));
    });

    $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' to ' + picker.endDate.format('DD/MM/YYYY'));
    });

    $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });

});