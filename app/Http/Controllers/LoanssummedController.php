<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;



use Symfony\Component\HttpFoundation\StreamedResponse;
use App\Statistic;
use App\Loanssummed;
use App\LoanStatement;
use Carbon\Carbon;
use DB;
use Response;
use App\User;

class LoanssummedController extends Controller
{
    //


    public function createLoansummed(){
    	//get the last rec_id
    	$last = Loanssummed::orderBy('rec_id', 'DESC')->first();
    	$id = isset($last->rec_id) ? $last->rec_id:0;
    	//dd($id);
    	//get the data from AX
    	$arr=array('rec_id' => $id, 'viewname' => 'loandt');
		$action='callviewsummedloans';
		//$action='getloaneepersonaldet';
		$loans = $this->doAPIcall($arr,$action);
		//dd($loans);
		$data = [];
		if(count($loans)){
		foreach ($loans as $key => $loan) {
			
			$record = [
				"rec_id" => $loan->rec_id,
				"username" => $loan->username,
			    "account_num" => $loan->account_num,
			    "id_no" => $loan->id_no,
			    "loan_category" => $loan->loan_category,
			    "loan_category_code" => $loan->loan_category_code,
			    "loan_serial_num" => $loan->loan_serial_num,
			    "academic_year" => $loan->academic_year,
			    "student_reg_num" => $loan->student_reg_num,
			    "university" => $loan->university,
			    'institution_code' => $loan->institution_code,
			    "outstanding_balance" => $loan->outstanding_balance,
			    "principal_disbursed" => $loan->principal_disbursed,
			    //"principal_disbursed" => $loan->principal_disbursed,
			    //'total_amount_paid' => $loan->total_amount_paid,
			    'start_year' => $loan->start_year,
			    'end_year' => Carbon::parse($loan->end_year)->format('Y'),
			    'loan_disbursement_date' => $loan->disbursementdate,
			    'loan_maturity_date' => $loan->loan_maturity_date,
			    'is_matured' => $loan->is_matured,
			    'phone_num' => $loan->phone_num,
			    'email' => $loan->email,
			    'statements_migration_status' => 0
			];
			Loanssummed::insert([$record]);
		}
	}
		//$data = $this->stdObjectToArray($loans);
		//dd($data);
    	//save the data to the db
    	//Loan::insert($data);
    	return 'Done';

    }

     public function doAPIcall($arr,$action) {

       	//$url = "http://197.136.52.3:1924/esb.php?rquest=$action";
        $url = "http://192.168.1.96:1924/esb.php?rquest=$action";

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($arr));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response);
        //dd($response);
        return $response;
    }


}
