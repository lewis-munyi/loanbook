<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use function MongoDB\BSON\toJSON;
use Symfony\Component\HttpFoundation\StreamedResponse;
use App\Statistic;
use App\Loan;
use App\newLoan;
use App\LoanStatement;
use App\Loanbillingheader;
use App\Loanbillinglines;
use App\Loancheckoffheader;
use App\Loancheckofflines;
use App\LoansLastPrincipalPaid;
use App\Collectionreportheader;
use App\Collectionperemployer;
use Carbon\Carbon;
use DB;
use Response;
use App\User;
use Auth;
use PDF;


class ReportsController extends Controller
{
    
 
public function collectionsReportFilter(Request $request){
      $loanStatement = new LoanStatement;
      $collectionreportfilter = new Collectionreportheader;
      $summary = array( );
      $persectors = array( );
      $this->validate($request, [
         'startDate' => 'required',
         'endDate' => 'required'
        ]);

    $startDate = date('Y-m-d', strtotime($request->input('startDate')));
    $endDate = date('Y-m-d', strtotime($request->input('endDate')));

    $loanproduct = $request->input('loanproduct');
    $sector = $request->input('sector');
    $arr=array('startDate' =>$startDate,'endDate' =>$endDate);
      
    #-------
    $action='getcollectionsinjournaltrans';
    $results = $loanStatement->doAPIcall($arr,$action);
    #-------
   // dd($results);
    if($results){
      foreach ($results as $key => $result) {
            $collectionperemployer =new Collectionperemployer;   
            $collectionperemployer->totalamount = number_format($result->amount, 2, '.', '');
            $collectionperemployer->accountnum = $result->accountnum;
            $collectionperemployer->transdate = $result->transdate;
            $collectionperemployer->name = $result->name==""?"-":$result->name;
            $collectionperemployer->save();
         }
        }
    dd($results);
    $collectionreportfilter->createHeader($headers,$arr);
    return view('collectionreport',['headers'=>$headers,'arr'=>$arr]);
}

 
public function collectionsReportFilterold(Request $request){
      $loanStatement = new LoanStatement;
      $collectionreportfilter = new Collectionreportfilter;
      $summary = array( );
    $persectors = array( );
    $this->validate($request, [
       'startDate' => 'required',
       'endDate' => 'required'
      ]);
//    todo : Change this date format
    $startDate = date('Y-m-d', strtotime($request->input('startDate')));
    $endDate = date('Y-m-d', strtotime($request->input('endDate')));

    $loanproduct = $request->input('loanproduct');
    $sector = $request->input('sector');
    $arr=array('startDate' =>$startDate,'endDate' =>$endDate,'sector' =>$sector,'loanproduct' =>$loanproduct);
       //dd($arr);
    $action='getcollectionreport';
    $summary = $loanStatement->doAPIcall($arr,$action);
    //dd($summary);
    #-------
    $action='getcollectionreportpersector';
    $persectors = $loanStatement->doAPIcall($arr,$action);
    #-------
    $action='getcollectionreportperproduct';
    $prods = $loanStatement->doAPIcall($arr,$action);
    #-------
    return view('collectionreport')->with('persectors',$persectors)->with('summary',$summary)->with('prods',$prods)->with('arr',$arr);
}

   public function reportPage(){
    return view('report');
   }
   public function getReport(Request $request){
      //dd($request->all());
        $limit = (int)$request['limit'];
        $loans = Loan::limit($limit)->select('rec_id', 'username', 'account_num', 'id_no')->get();
        return response()->json($loans->toArray());

   }
   
    public function collectionsReport(){
        $persectors= array();
        //dd($persectors);
        $summary= array();
        $prods= array();
        return view('collectionreport')->with('persectors',$persectors)->with('summary',$summary)->with('prods',$prods);

    
    
    }
  public function checkoffcron(Request $request){
    $loancheckoffheader = new Loancheckoffheader();
    $loanStatement = new LoanStatement();
    $loancheckofflines=  new Loancheckofflines();
     $last = Loancheckoffheader::orderBy('recid', 'DESC')->first();
     $recid = isset($last->recid) ? $last->recid:0;
     $action='checkoffcron';
     $arr=array('recid' =>$recid);
     $response = $loanStatement->doAPIcall($arr,$action);
  //dd($response);
     foreach ($response as $key => $res) {
     #create the header record      
       $header_id= $loancheckoffheader->createHeader($res);
     #create the lines
       $id= $loancheckofflines->createlines($res,$header_id);
                                          }
    
 

  }
 public function billingReportCron(Request $request){
    $loanbillingheader = new Loanbillingheader();
    $loanStatement = new LoanStatement();
     $loanbillinglines=  new Loanbillinglines();

      $last = Loanbillingheader::orderBy('recid', 'DESC')->first();
      $recid = isset($last->recid) ? $last->recid:0;
     
    $action='billingreportcron';
     $arr=array('recid' =>$recid);
    $response = $loanStatement->doAPIcall($arr,$action);
//dd($response);
     foreach ($response as $key => $res) {
     #create the header record      
       $header_id= $loanbillingheader->createHeader($res);
     #create the lines
       $header_id= $loanbillinglines->createlines($res,$header_id);
                                          }
    
 

  }
  public function billingReport(){
      $persectors= array();
      $billingheaders= array();
      $prods= array();
      $h=0;
      $noth=0;
      $hcount=0;
      $nothcount=0;
       $randnum=0;
       $tarehe='';
       $empcode='';
       $officer='';

        return view('billingreport',['tarehe'=>$tarehe,'empcode'=>$empcode,'officer'=>$officer])->with('persectors',$persectors)->with('billingheaders',$billingheaders)->with('prods',$prods);

    
    
    }
    public function billingReportFilter(Request $request){
        $loanbillingheader = new Loanbillingheader();
      $h=0;
      $noth=0;
       $this->validate($request, [
        'datefilter' => 'required' 
      ]);
       $tarehe =request('datefilter');
        $dates = explode("to", preg_replace('/\s+/', '', request('datefilter')));
        $startDate = Carbon::createFromFormat('d/m/Y', $dates[0]);
        $endDate = Carbon::createFromFormat('d/m/Y', $dates[1]);
        $empcode = $request->input('empcode');
        $officer = $request->input('officer');
        $billingheaders= $loanbillingheader->getreportdata($startDate,$endDate,$empcode,$officer);

       
 
          foreach ($billingheaders as $key => $billingheader) {
            $h=$h+$billingheader->billingLines->where('bill_honored', 1)->count();
            $noth=$noth+$billingheader->billingLines->where('bill_honored', 0)->count();

          }

    return view('billingreport',
    ['billingheaders' => $billingheaders,'randnum'=>0,'h'=>$h,'noth'=>$noth,'tarehe'=>$tarehe,'empcode'=>$empcode,'officer'=>$officer]);

    }


 public function dropoutsReport(Request $request){
        $loanbillingheader = new Loanbillingheader();
        $loancheckoffheader =new Loancheckoffheader();
      $h=0;
      $noth=0;
      $results = $loancheckoffheader->getdropouts($startDate,$endDate,$empcode,$officer);
       $res  = Loanbillingheader::whereBetween('datecreated', array($startDate, $endDate))->get();
          return $res; 
       
                                
   
    //dd( $billingheaders);
        foreach ($billingheaders as $key => $billingheader) {
          $h=$h+$billingheader->billingLines->where('bill_honored', 1)->count();
          $noth=$noth+$billingheader->billingLines->where('bill_honored', 0)->count();
        }

    return view('billingreport',
    ['billingheaders' => $billingheaders,'randnum'=>0,'h'=>$h,'noth'=>$noth]);

    }


    public function billingReportFilterold(Request $request){
      $loanStatement = new LoanStatement;
      $billingheaders = array( );
      $randnum=mt_rand();
      $h=0;
      $noth=0;
      $hcount=0;
      $nothcount=0;
    $persectors = array( );
    $this->validate($request, [
        'startDate' => 'required' ,
        'endDate' => 'required' ,
      ]);
//		todo : Change this date format
        $dates = explode("to", preg_replace('/\s+/', '', request('datefilter')));
        $startDate = Carbon::createFromFormat('d/m/Y', $dates[0]);
        $endDate = Carbon::createFromFormat('d/m/Y', $dates[1]);

//       $startDate = date('Y-m-d', strtotime($request->input('startDate')));
//       $endDate = date('Y-m-d', strtotime($request->input('endDate')));
       $empcode = $request->input('empcode');
       $officer = $request->input('officer');
       $arr=array('startDate' =>$startDate,'endDate' =>$endDate,'officer' =>$officer,'empcode' =>$empcode);
    $action='billingreport';
    $billingheaders = $loanStatement->doAPIcall($arr,$action);
    $loanbillingheader = new Loanbillingheader();
    #create headers
    if ($billingheaders) {
      foreach ($billingheaders as $key => $billingheader) {
       $header_id= $loanbillingheader->createHeader($billingheader,$randnum);

       #create the lines
       $loanbillinglines=  new Loanbillinglines();
       $action='getbillinglinesbybtachno';
       $arr=array('batchno' =>$billingheader->batchno);
       $billinglines = $loanStatement->doAPIcall($arr,$action);
      if($billinglines){
        $data=array();
        foreach ($billinglines as $key => $line) {
          # check if it being honored

             $action='getlastrepaymentdatePDO';
             $arr=array('accountnum' =>$line->ACCOUNTNUM);
             $lastpaydate = $loanStatement->doAPIcall($arr,$action);
        $lastpaydate;
        $billhonored=$lastpaydate>$billingheader->datedue?1:0;
        $record = [
                            "loanbillingheaders_id"=>$header_id,
                            "accountnum"=>$line->ACCOUNTNUM,
                            "idno" => $line->NATIONALIDNO,
                            "lastpaydate" => $line->LASTPAYMENTDATE,
                            "loanproductcode" => $line->LOANPRODUCTCODE,
                            "loanserialno" => $line->LOANSERIALNO,
                            "outstanding_balance" => $line->OUTSTANDINGBALANCE,
                            "repaymentrate" => $line->RATEOFPAYMENT,
                            "bill_honored" => $billhonored,
                            "randnum" => $randnum,
                            "lastpaydateafterbill" => $lastpaydate
                        ];
                        
                        $data[] = $record;
      }
      Loanbillinglines::insert($data);
      }
     
                            

      }

    $h = Loanbillinglines::where('randnum', $randnum)
                                   ->where('bill_honored', 1)
                                   ->get();
                                   
    $hcount= count($h)  ;                               
    
    $noth = Loanbillinglines::where('randnum', $randnum)
                                   ->where('bill_honored', 0)
                              ->get();
    $nothcount= count($noth)  ; 
                                   
     $billingheaders = $loanbillingheader->where('randnum', $randnum)->orderBy('officer')->get();    
 }
    #-------
  return view('billingreport',
    ['billingheaders' => $billingheaders,'h'=>$h,'noth'=>$noth,'randnum'=>$randnum,
    'hcount'=>$hcount,'nothcount'=>$nothcount]);

    
   }


    public function billsnothonoredCSV(){
        //////////////////////////////////////
        $response = new StreamedResponse(function(){
            // Open output stream
            $handle = fopen('php://output', 'w');

                fputcsv($handle, array('SNo.','Student Name','phone','email', 'ID No.', 'Admission No.', 'University', 'Product', 'Academic Yr.', 'university', 'Start Yr.', 'End Yr.', 'Maturity Date','Principal', 'Principal repaid', 'interest charged', 'interest repaid', 'ledger charged', 'ledger repaid', 'insurance charged', 'insurance repaid', 'penalty charged', 'penalty repaid','waivers','Running Bal'));
            //$loans = Loan::has('statements')->skip(0)->limit(500)->get();
            // Get all users

            Loan::defaulted()->chunk(5000, function($loans) use($handle){
            foreach ($loans as $loan) {
                // Add a new row with data
                fputcsv($handle, [
                    $loan->loan_serial_num,
                    $loan->username,
                    $loan->phone_num,
                    $loan->email,
                    $loan->id_no,
                    $loan->student_reg_num,
                    $loan->institution_code,
                    $loan->loan_category_code,
                    $loan->academic_year,
                    $loan->university,
                    $loan->start_year,
                    $loan->end_year,
                    $loan->loan_maturity_date,
                    $loan->principal_disbursed,
                    $loan->principal_repaid,
                    $loan->interest_charged,
                    $loan->interest_repaid,
                    $loan->ledger_charged,
                    $loan->ledger_repaid,
                    $loan->insurance_charged,
                    $loan->insurance_repaid,
                    $loan->penalty_charged,
                    $loan->penalty_repaid,
                    $loan->waivers_amount,
                    $loan->running_balance
                    //$loan->outstanding_balance
                              ]);
            }//end foreach
        });

            // Close the output stream
            fclose($handle);
        }, 200, [
                'Content-Type' => 'text/csv',
                'Content-Disposition' => 'attachment; filename="defaultersList.csv"',
            ]);

        return $response->send();
        /////////////////////////////////////////////////
    }

   
}
