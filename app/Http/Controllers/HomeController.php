<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Statistic;
use App\Loan;

class HomeController extends Controller
{
    
    public function index(){
        $loanCategories = Loan::select(array ('loan_category_code','loan_category'))->distinct()->orderBy('loan_category_code','asc')->get();
        $academicYears = Loan::select('academic_year')->distinct()->orderBy('academic_year','desc')->get();
        $categorycode = '';
        $acadyr = '';
        // dd($loanCategories);
       	$stats = [
    		'all_count' => Loan::count(),
    		'all_amount' => Loan::sum('principal_disbursed'),
    		'matured_count' => Loan::matured()->count(),
    		'matured_amount' => Loan::matured()->sum('principal_disbursed'),
    		'unmatured_count' => Loan::unmatured()->count(),
    		'unmatured_amount' => Loan::unmatured()->sum('principal_disbursed'),
            'cleared_count' => Loan::cleared()->count(),
            'running_count' => Loan::running()->count(),
            'pl_count' => Loan::performing()->count(),
            'pl_amount' => Loan::performing()->sum('principal_disbursed'),
            'npl_count' => Loan::matured()->npl()->count(),
            'npl_amount' => Loan::matured()->npl()->sum('principal_disbursed'),
            'dormant_count' => Loan::matured()->npl()->dormantloans()->count(),
            'dormant_amount' => Loan::matured()->npl()->dormantloans()->sum('principal_disbursed'),
            'defaulted_count' => Loan::defaulted()->count(),
            'defaulted_amount' => Loan::matured()->npl()->defaulted()->sum('principal_disbursed')
    	];
    	//$stats = Statistic::first();
        return view('home')->with('stats', (object)$stats)->with('loanCategories', (object)$loanCategories)
        ->with('academicYears', $academicYears)->with('categorycode',$categorycode)->with('acadyr',$acadyr);
    }

    //Funciton to return filtered data
    public function filterResults(Request $request){
        $loanCategories = Loan::select(array ('loan_category_code','loan_category'))->distinct()->get();
        $academicYears = Loan::select('academic_year')->distinct()->get();
        // dd($request->all());
        $loan_category_code = request('product');
        $academicYear = request('academic_year');
    if(isset($loan_category_code) && $loan_category_code!='' && $academicYear =='' )
    {

         $stats = [
        'all_count' => Loan::where('loan_category_code', $loan_category_code)->count(),
        'all_amount' => Loan::where('loan_category_code', $loan_category_code)->sum('principal_disbursed'),
        'matured_count' => Loan::where('loan_category_code', $loan_category_code)->matured()->count(),
        'matured_amount' => Loan::where('loan_category_code', $loan_category_code)->matured()->sum('principal_disbursed'),
        'unmatured_count' => Loan::where('loan_category_code', $loan_category_code)->unmatured()->count(),
        'unmatured_amount' => Loan::where('loan_category_code', $loan_category_code)->unmatured()->sum('principal_disbursed'),
            'cleared_count' => Loan::where('loan_category_code', $loan_category_code)->cleared()->count(),
            'running_count' => Loan::where('loan_category_code', $loan_category_code)->running()->count(),
            'pl_count' => Loan::where('loan_category_code', $loan_category_code)->performing()->count(),
            'pl_amount' => Loan::where('loan_category_code', $loan_category_code)->performing()->sum('principal_disbursed'),
            'npl_count' => Loan::where('loan_category_code', $loan_category_code)->npl()->count(),
            'npl_amount' => Loan::where('loan_category_code', $loan_category_code)->npl()->sum('principal_disbursed'),
            'dormant_count' => Loan::where('loan_category_code', $loan_category_code)->dormantloans()->count(),
            'dormant_amount' => Loan::where('loan_category_code', $loan_category_code)->dormantloans()->sum('principal_disbursed'),
            'defaulted_count' => Loan::where('loan_category_code', $loan_category_code)->defaulted()->count(),
            'defaulted_amount' => Loan::where('loan_category_code', $loan_category_code)->defaulted()->sum('principal_disbursed')
      ];

    }else{
        if(isset($academicYear) && $academicYear!='' && $loan_category_code =='' )
    {

 $stats = [
        'all_count' => Loan::where('academic_year', $academicYear)->count(),
        'all_amount' => Loan::where('academic_year', $academicYear)->sum('principal_disbursed'),
        'matured_count' => Loan::where('academic_year', $academicYear)->matured()->count(),
        'matured_amount' => Loan::where('academic_year', $academicYear)->matured()->sum('principal_disbursed'),
        'unmatured_count' => Loan::where('academic_year', $academicYear)->unmatured()->count(),
        'unmatured_amount' => Loan::where('academic_year', $academicYear)->unmatured()->sum('principal_disbursed'),
            'cleared_count' => Loan::where('academic_year', $academicYear)->cleared()->count(),
            'running_count' => Loan::where('academic_year', $academicYear)->running()->count(),
            'pl_count' => Loan::where('academic_year', $academicYear)->performing()->count(),
            'pl_amount' => Loan::where('academic_year', $academicYear)->performing()->sum('principal_disbursed'),
            'npl_count' => Loan::where('academic_year', $academicYear)->npl()->count(),
            'npl_amount' => Loan::where('academic_year', $academicYear)->npl()->sum('principal_disbursed'),
            'dormant_count' => Loan::where('academic_year', $academicYear)->dormantloans()->count(),
            'dormant_amount' => Loan::where('academic_year', $academicYear)->dormantloans()->sum('principal_disbursed'),
            'defaulted_count' => Loan::where('academic_year', $academicYear)->defaulted()->count(),
            'defaulted_amount' => Loan::where('academic_year', $academicYear)->defaulted()->sum('principal_disbursed')
      ];

    }else{
        if(isset($academicYear) && $academicYear!='' && isset($loan_category_code) && $loan_category_code !='' ){ 
      $stats = [
        'all_count' => Loan::where('academic_year', $academicYear)
                            ->where('loan_category_code', $loan_category_code)->count(),
        'all_amount' => Loan::where('academic_year', $academicYear)
                            ->where('loan_category_code', $loan_category_code)->sum('principal_disbursed'),
        'matured_count' => Loan::where('academic_year', $academicYear)
                            ->where('loan_category_code', $loan_category_code)->matured()->count(),
        'matured_amount' => Loan::where('academic_year', $academicYear)
                                ->where('loan_category_code', $loan_category_code)->matured()->sum('principal_disbursed'),
        'unmatured_count' => Loan::where('academic_year', $academicYear)
                                ->where('loan_category_code', $loan_category_code)->unmatured()->count(),
        'unmatured_amount' => Loan::where('academic_year', $academicYear)
                                ->where('loan_category_code', $loan_category_code)->unmatured()->sum('principal_disbursed'),
            'cleared_count' => Loan::where('academic_year', $academicYear)
                                    ->where('loan_category_code', $loan_category_code)->cleared()->count(),
            'running_count' => Loan::where('academic_year', $academicYear)
                                    ->where('loan_category_code', $loan_category_code)->running()->count(),
            'pl_count' => Loan::where('academic_year', $academicYear)
                                ->where('loan_category_code', $loan_category_code)->performing()->count(),
            'pl_amount' => Loan::where('academic_year', $academicYear)
                                ->where('loan_category_code', $loan_category_code)->performing()->sum('principal_disbursed'),
            'npl_count' => Loan::where('academic_year', $academicYear)
                                ->where('loan_category_code', $loan_category_code)->npl()->count(),
            'npl_amount' => Loan::where('academic_year', $academicYear)
                                ->where('loan_category_code', $loan_category_code)->npl()->sum('principal_disbursed'),
            'dormant_count' => Loan::where('academic_year', $academicYear)
                                    ->where('loan_category_code', $loan_category_code)->dormantloans()->count(),
            'dormant_amount' => Loan::where('academic_year', $academicYear)
                                    ->where('loan_category_code', $loan_category_code)->dormantloans()->sum('principal_disbursed'),
            'defaulted_count' => Loan::where('academic_year', $academicYear)
                                    ->where('loan_category_code', $loan_category_code)->defaulted()->count(),
            'defaulted_amount' => Loan::where('academic_year', $academicYear)
                                    ->where('loan_category_code', $loan_category_code)->defaulted()->sum('principal_disbursed')
      ];
  }
  }
}

   //Fetch form input
      //loans = all loans where product and academic year in loans collumn matches
      //input
      //dd(json_encode($loans));
      return view('home')->with('stats', (object)$stats)->with('loanCategories', (object)$loanCategories)
      ->with('academicYears', $academicYears)->with('categorycode',$loan_category_code)->
      with('acadyr', $academicYear);
    }

    public function landing(){
        return view('landingpage');
    }
}
