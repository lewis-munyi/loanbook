<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoansLastPrincipalPaid extends Model
{
    //
    protected $table = 'loans_last_principal_paid';
    
    public function loan(){
    	return $this->belongsTo('App\Loan','account_num','accountnum');
    }
}
