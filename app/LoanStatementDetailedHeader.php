<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class LoanStatementDetailedHeader extends Model
{
    //
     public function createHeader($arr) {
     //	dd($arr);
      

     	 $record = [
                            "names"=>$arr->name,
                            "idnumber" => $arr->idnumber,
                            "loanproductcode" => $arr->loanproductcode,
                            "PRODUCTDESCRIPTION" => $arr->PRODUCTDESCRIPTION,
                            "loanserialno" => $arr->loanserialno,
                            "accountnum" => $arr->accountnum,
                            "loanprincipal" => number_format($arr->LOANPRINCIPALAMOUNT, 2, '.', ''),
                            "outstanding_interest" => number_format($arr->OUTSTANDINGLOANINTEREST, 2, '.', ''),
                            "outstanding_principal" => number_format($arr->OUTSTANDINGPRINCIPALBALANCE, 2, '.', ''),
                            "outstanding_insurance" => number_format($arr->OUTSTANDINGLOANINSURANCE, 2, '.', ''),
                            "outstanding_ledgerfee" =>  number_format($arr->OUTSTANDINGLOANLEDGERFEE, 2, '.', ''),
                            "outstanding_penalty" => number_format($arr->OUTSTANDINGLOANPENALTY, 2, '.', '') ,
                      		"running_balance" => number_format($arr->RUNNINGLOANBALANCE, 2, '.', ''),
                        ];
                      

       return $id= $this->insertGetId($record); 
    }
      public function statementLines(){
    	return $this->hasMany('App\LoanStatementDetailed','loan_statement_detailed_headers_id')->orderBy('trans_period', 'asc');;
    }

    public function statementLinesGrouped()
{
 
     return $this->hasMany('App\LoanStatementDetailed','loan_statement_detailed_headers_id')
                    ->select(DB::raw('journalnum,max(documentnum) as documentnum,max(transaction_date) as transaction_date,max(paymode) as paymode ,max(originator) as originator ,max(transaction_type) as transaction_type,max(transaction_description) as transaction_description, max(transaction_period) as transaction_period,sum(credit_amount) as credit_amount,sum(debit_amount) as debit_amount'))
                    ->groupBy('journalnum')
                    ->orderBy('trans_period', 'asc');
}


}
